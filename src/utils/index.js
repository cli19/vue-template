// Utils maven
Date.prototype.format = function (fmt) {   // eslint-disable-line
  const o = {
    'M+': this.getMonth() + 1, // 月份
    'd+': this.getDate(), // 日
    'h+': this.getHours(), // 小时
    'm+': this.getMinutes(), // 分
    's+': this.getSeconds(), // 秒
    'q+': Math.floor((this.getMonth() + 3) / 3), // 季度
    S: this.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (const k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}

// 格式化时间
export const formatDate = (date = new Date(), formatStr = 'yyyy-MM-dd') => {
  if (!date) {
    return '-'
  } else {
    if (/1-01-01T00:00:00Z/.test(date)) return '-'
    const currentDate = new Date(date).format(formatStr)
    if (/NaN-aN-aN/.test(currentDate)) return '-'
    return currentDate
  }
}

// 截流函数
export const delay = (function () {
  let timer = 0
  return function (callback, ms) {
    clearTimeout(timer)
    timer = setTimeout(callback, ms)
  }
})()
