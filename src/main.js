import Vue from 'vue'
import App from './App.vue'
import { router } from './router'
import filter from './filter'
import directive from './directive'
import store from './store'

Vue.config.productionTip = false

Vue.use(filter)
Vue.use(directive)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
