import Vue from 'vue'

const eventQueue = new Map() // 事件队列

const VueBus = new Vue()

export const $bus = VueBus

// 错误监听
VueBus.$on('requestError', (error) => {
  const { request, response } = error
  if (request) { // 请求错误处理
  }
  if (response) { // 返回错误处理
  }
})

// 添加队列
export const addQueue = (name, event) => {
  eventQueue.set(name, event)
}

// 删除队列
export const removeQueue = (name) => {
  eventQueue.delete(name)
}

// 执行指定队列中的事件
export const actionQueue = (name, ...rest) => {
  const event = eventQueue.get(name)
  if (!event) {
    throw Error('event is undefiend')
  }
  return event(rest)
}

export default VueBus
