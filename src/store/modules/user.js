const state = {}
const mutations = {}
const actions = {}

export const user = {
  namespaced: true,
  state,
  mutations,
  actions
}
