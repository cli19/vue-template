module.exports = {
  root: true,
  env: {
    node: true
  },
  globals: {}, // 全局变量
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-irregular-whitespace': 'off', // 多余的空格
    'space-before-function-paren': 'off',
    'css-ruleorselectorexpected': 'off', // 关闭没有 css 中开头不是选择
    'x-invalid-end-tag': 'off' // 插件回去多余判断字符串中的字符标签，标签闭合
  },
}
