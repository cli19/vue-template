const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin') // 去掉注释
const CompressionWebpackPlugin = require('compression-webpack-plugin') // 开启压缩
const { HashedModuleIdsPlugin, BannerPlugin } = require('webpack')
const Happypack = require('happypack')
const WebpackVersionPlugin = require('./plugin/VersionPlugin')
const DeploymentPlugin = require('./plugin/DeploymentPlugin')
const logConfig = require('./logger.config')
const devConfig = require('./deploy.config')

const isProduction = process.env.NODE_ENV === 'production'
const productionGzipExtensions = /\.(js|css|json|txt|html|ico|svg)(\?.*)?$/

// cdn预加载使用
const externals = {
  'vue': 'vue'
}

const cdn = {
  // 开发环境
  dev: {
    css: [],
    js: []
  },
  // 生产环境
  build: {
    css: [],
    js: []
  }
}

module.exports = {
  productionSourceMap: !isProduction, // 生成环境不需要源码映射
  publicPath: '/',
  outputDir: logConfig.genVersionDir(),
  chainWebpack: config => {
    // main是入口js文件
    config.entry('main').add('babel-polyfill')
    // webpack 会默认给commonChunk打进chunk-vendors，所以需要对webpack的配置进行delete
    config.optimization.delete('splitChunks')
    config.plugin('html').tap(args => {
      args[0].cdn = isProduction ? cdn.build : cdn.dev
      return args
    })
    // Markdown 转 vue 组件配置
    // config.module.rule('md')
    //   .test(/\.md/)
    //   .use('vue-loader')
    //   .loader('vue-loader')
    //   .end()
    //   .use('vue-markdown-loader')
    //   .loader('vue-markdown-loader/lib/markdown-compiler')
    //   .options({
    //     raw: true
    //   })
    if (isProduction) {
      // 分析 webpack 代码库拆分块
      // config
      //   .plugin('webpack-bundle-analyzer')
      //   .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
    }
    return config
  },
  configureWebpack: config => {
    const plugins = []
    if (isProduction) {
      plugins.push(
        // 版本日志
        new WebpackVersionPlugin(logConfig.file, logConfig),
        // 自动部署
        new DeploymentPlugin(devConfig)
      )
      // 取消注释和打印
      plugins.push(
        new UglifyJsPlugin({
          include: /src/,
          uglifyOptions: {
            output: {
              comments: true // 去掉注释
            },
            warnings: true,
            compress: {
              drop_console: true,
              drop_debugger: false,
              pure_funcs: ['console.log'] // 移除console
            }
          }
        })
      )
      // 服务器也要相应开启gzip
      plugins.push(
        // https://blog.csdn.net/qq_45062261/article/details/107877136
        new CompressionWebpackPlugin({
          filename: '[path].gz[query]', //目标资源名称
          algorithm: 'gzip',
          // test: /\.(js|css)$/, // 匹配文件名
          test: productionGzipExtensions, // 匹配文件名
          threshold: 10 * 1024, // 对超过10k的数据压缩
          deleteOriginalAssets: false, // 不删除源文件
          minRatio: 0.8 // 压缩比
        })
      )
      // 用于根据模块的相对路径生成 hash 作为模块 id, 一般用于生产环境
      plugins.push(
        new HashedModuleIdsPlugin()
      )
      plugins.push(
        new BannerPlugin('@author Minyoung & wlittleyang@163.com')
      )
      plugins.push(
        new Happypack({
          threads: 6,
          loaders: [ 'vue-loader', 'babel-loader', 'css-loader', 'less-loader', 'style-resources-loader' ]
        })
      )
      // 开启分离js
      config.optimization = {
        runtimeChunk: 'single',
        splitChunks: {
          chunks: 'all',
          // 入口点的最大并行请求数
          maxInitialRequests: 30,
          minSize: 1000 * 60,
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                // 排除node_modules 然后吧 @ 替换为空 ,考虑到服务器的兼容
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
                let customName = packageName.replace('@', '')
                return `npm.${customName}`
              }
            }
          }
        }
      }
      // 取消webpack警告的性能提示
      config.performance = {
        hints: 'warning',
        // 入口起点的最大体积
        maxEntrypointSize: 1000 * 5000,
        // 生成文件的最大体积
        maxAssetSize: 1000 * 1000,
        // 只给出 js 文件的性能提示
        assetFilter: function (assetFilename) {
          return assetFilename.endsWith('.js')
        }
      }
      config.externals = externals
    }
    return { plugins }
  },
  // 代理
  // devServer: {
  //   historyApiFallback: true,
  //   proxy: {
  //     '/api': {
  //       target: ',
  //       pathRewrite: {'^/api' : ''},
  //       changeOrigin: true
  //     }
  //   }
  // },
  pluginOptions: {
    // 模块注入 全局less
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [
        path.resolve(__dirname, './src/style/variable.less'),
        path.resolve(__dirname, './src/style/index.less')
      ]
    }
  }
}
