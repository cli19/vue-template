import * as filters from './filters'

export default (Vue) => {
  Object.keys(filters).map(key => {
    Vue.filter(key, filters[key])
  })
}
