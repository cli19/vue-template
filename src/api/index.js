/**********************************************/
/*                  API 地址                  */
/*  stage:http://[host]/swagger/index.html    */
/*  build:http://[host]/swagger/index.html    */
/*                                            */
/**********************************************/

import Axios from 'axios'
import Qs from 'qs'
// import cookies from 'vue-cookies'
import $bus from '@/utils/bus'
import { delay } from '@/utils'

Axios.defaults.withCredentials = true // 让ajax携带cookie

const IS_PROD = process.env.NODE_ENV === 'production'
const loginReg = /(\/login\?)|(\/login$)/
const CancelToken = Axios.CancelToken
const pending = [] // 存放已发送未完成的请求

// 取消请求
function cancel(conf) {
  pending.forEach((item, index) => {
    if (item.p + item.m === conf.url + conf.method) {
      item.c('CanceledRequest', item.toString()) // 符合规则的取消请求
      pending.splice(index, 1)
    }
  })
}

// 请求错误处理
const createError = (error) => {
  $bus.$emit('createError', Object.create(error))
}

// 创建请求对象
export const $http = Axios.create({
  timeout: 15000,
  baseURL: `https://${IS_PROD ? 'deveploment.api' : 'production.api'}.com`,
  withCredentials: true,
  headers: {
    // 'Content-Type': 'application/json'
    'Content-Type': 'application/x-www-form-urlencoded' // charset=UTF-8
  }
})

// 请求拦截
$http.interceptors.request.use(
  config => { 
    cancel(config) // 取消上一次请求
    config.cancelToken = new CancelToken(c => {
      pending.push({
        p: config.url,
        m: config.method,
        c
      })
    })
    // const token = cookies.get('token')
    // 非登录页面 cookies拦截
    // if (!loginReg.test(config.url) && !token) {
    //   alert('请重新登录')
    // }
    // 携带 token 具体根据后端提供的规则而定
    config.headers['token'] = token
    if (config.headers['Content-Type'] === 'application/x-www-form-urlencoded') {
      config.data = Qs.stringify(config.data)
    }
    return config
  },
  error => {
    // 请求错误处理
    console.log('axios error message:', error) // eslint-disable-line
  }
)

// 返回拦截
$http.interceptors.response.use(
  response => { // 200
    cancel(response.config)
    const config = response.config
    if (loginReg.test(config.url)) {
      if (response.data.code !== 200) {
        createError({ response })
        return response
      }
      // const token = response.headers['authorization']
      // cookies.set('token', token, '10y')
    }
    return response.data
  },
  error => {
    // 断网处理
    if (!error.response || !error.message) {
      error.message !== 'CanceledRequest' && delay(() => {
        alert('与服务器断开连接~')
      }, 150)
      return
    }
    if (error.response.status === 401) { // token失效 退出登录
      alert('登录过期，请重新登录！')
    }
    if (error.response.status === 500) {
      alert('服务器内部错误！')
    }
    return error.response && error.response.data
  }
)
