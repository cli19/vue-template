const path = require('path')
const { version } = require('./package.json')

module.exports = {
  version,
  file: path.resolve('build/version.log'),
  upgrade: '', // 主版本
  addition: '', // 子版本
  fix: '打包证书目录结构更新', // 修复bug版本
  // 创建版本文件夹
  genVersionDir() {
    // 主版本号.子版本号.修正版本号.[a-c][正整数]
    return `build/version_${version}`
  }
}