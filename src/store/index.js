import Vue from 'vue'
import Vuex from 'vuex'
import { modules } from './modules'
import { version } from '../../package.json'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    version
  },
  mutations: {
  },
  actions: {
  },
  modules
})
