# <%=projectName%>

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn dev
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### deploy.config.js
webpack 自动部署插件配置，特别地用于 centos
```js
// ~/plugin/VersionPlugin
module.exports = {
  host: '', // 主机 ip
  port: '22', // 端口
  username: 'root', // ssh 链接用户名
  password: '', // ssh 链接密码
	remoteDir: '/www/wwwroot/<%=projectName%>', // 上传带服务器的位置
	// onlyCompress: true // 只打包 不上传
}
```