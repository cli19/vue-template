import Home from '@/views/Home'

// 动态路由
export const dynamicRoutes = [
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/About')
  }
]

export const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  ...dynamicRoutes
]
