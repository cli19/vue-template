export default (Vue) => {
  // v-focus 获取焦点
  Vue.directive('focus', {
    inserted: function(el) {
      const tags = ['textarea', 'input']
      if (tags.includes(el.tagName.toLowerCase())) {
        el.focus()
        return
      }
      const dom = el.querySelector('input') || el.querySelector('textarea')
      dom.focus()
    }
  })
}
